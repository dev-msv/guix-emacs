(define-module (devmsv packages xorg)  
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public xcb-proto-1.17
  (package
    (inherit xcb-proto)
    (version "1.17.0")))

(define-public libxcb-1.17
  (package
    (inherit libxcb)
    (version "1.17.0")
    (inputs (modify-inputs (package-inputs libxcb)
              (replace "xcb-proto" xcb-proto-1.17)))))
