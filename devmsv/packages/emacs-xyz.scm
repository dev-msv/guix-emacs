(define-module (devmsv packages emacs-xyz)
  #:use-module (devmsv packages emacs)
  #:use-module (devmsv packages xorg)
  #:use-module (guix licenses)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages djvu)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages enchant)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages xorg)
  #:use-module (guix gexp)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix transformations)
  #:use-module (guix utils))

(define (emacs-next-rename pkg-name)
  (if (string-prefix? "emacs" pkg-name)
      (string-append "emacs-next"
                     (string-drop pkg-name
                                  (string-length "emacs")))) pkg-name)

;; List of pkgs to exclude from devmsv-emacs re-compilation
;; dependencies not excluded
(define devmsv-emacs-pkg-excludes
  '("emacs-aio"
    "emacs-buttercup"
    "emacs-forge"
    "emacs-org"
    "emacs-notmuch"
    "emacs-ejira"))

(define-public emacs-next-exwm
  (package
    (inherit emacs-exwm)
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-exwm)
       ((#:emacs emacs) `,emacs-next)))
    (name "emacs-next-exwm")))

(define-public emacs-next-lucid-exwm
  (package
    (inherit emacs-exwm)
    (arguments
    (substitute-keyword-arguments (package-arguments emacs-exwm)
      ((#:emacs emacs) `,emacs-next-lucid)))
    (name "emacs-next-lucid-exwm")))

(define-public emacs-pretest-exwm
  (package
    (inherit emacs-exwm)
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-exwm)
       ((#:emacs emacs) `,emacs-pretest)))
    (name "emacs-pretest-exwm")))

(define-public emacs-master-exwm
  (package
    (inherit emacs-exwm)
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-exwm)
       ((#:emacs emacs) `,emacs-master)))
    (name "emacs-master-exwm")))

(define-public emacs-master-lucid-exwm
  (package
    (inherit emacs-exwm)
    (arguments
    (substitute-keyword-arguments (package-arguments emacs-master-exwm)
      ((#:emacs emacs-master) `,emacs-master-lucid)))
    (name "emacs-master-lucid-exwm")))

(define-public emacs-master-lucid-igc-exwm
  (package
    (inherit emacs-exwm)
    (arguments
    (substitute-keyword-arguments (package-arguments emacs-master-exwm)
      ((#:emacs emacs-master) `,emacs-master-lucid-igc)))
    (name "emacs-master-lucid-igc-exwm")))

(define-public emacs-pretest-lucid-exwm
  (package
    (inherit emacs-pretest-exwm)
    (arguments
    (substitute-keyword-arguments (package-arguments emacs-pretest-exwm)
      ((#:emacs emacs-pretest) `,emacs-pretest-lucid)))
    (name "emacs-pretest-lucid-exwm")))

(define-public devmsv-emacs-exwm
  (package
    (inherit emacs-master-lucid-igc-exwm)
    (name "devmsv-emacs-exwm")))

(define-public (package-with-emacs-next emacs-pkg)
  (if (include-devmsv-emacs emacs-pkg)
      ((package-input-rewriting `((,emacs unquote devmsv-emacs)
				  (,emacs-next unquote devmsv-emacs)
				  (,emacs-minimal unquote devmsv-emacs-minimal)
				  (,emacs-next-minimal unquote devmsv-emacs-minimal)
				  (,emacs-no-x unquote devmsv-emacs-no-x)
				  (,emacs-exwm unquote devmsv-emacs-exwm)
				  (,emacs-buttercup unquote emacs-buttercup-without-tests))
				emacs-next-rename)
       (package
	(inherit emacs-pkg)
	(arguments
	 (substitute-keyword-arguments (package-arguments emacs-pkg)
				       ((#:emacs emacs)
					`,emacs-next)
				       ((#:emacs emacs-minimal)
					`,emacs-next-minimal)))))
      emacs-pkg))

;; (define (emacs-master-rename pkg-name)
;;   (if (string-prefix? "emacs-next" pkg-name)
;;       (string-append "emacs-master"
;;                      (string-drop pkg-name
;;                                   (string-length "emacs-next")))
;;       (if (string-prefix? "emacs" pkg-name)
;;           (string-append "emacs-master"
;;                          (string-drop pkg-name
;;                                       (string-length "emacs"))) pkg-name)))

;; (define-public (package-with-emacs-master emacs-pkg)
;;   ((package-input-rewriting `((,emacs unquote emacs-master)
;;                               (,emacs-next unquote emacs-master)
;;                               (,emacs-minimal unquote emacs-master-minimal)
;;                               (,emacs-next-minimal unquote
;;                                emacs-master-minimal)) emacs-master-rename)
;;    (package
;;      (inherit emacs-pkg)
;;      (arguments
;;       (substitute-keyword-arguments (package-arguments emacs-pkg)
;;         ((#:emacs emacs)
;;          `,emacs-master)
;;         ((#:emacs emacs-minimal)
;;          `,emacs-master-minimal)
;;         ((#:emacs emacs-next)
;;          `,emacs-master)
;;         ((#:emacs emacs-next-minimal)
;;          `,emacs-master-minimal))))))

(define (devmsv-emacs-rename pkg-name)
  (if (string-prefix? "emacs-next" pkg-name)
      (string-drop pkg-name
                   (string-length "emacs-next"))
      (if (string-prefix? "emacs-pretest" pkg-name)
	  (string-drop pkg-name
                       (string-length "emacs-pretest"))
	  (if (string-prefix? "emacs" pkg-name)
              (string-append "devmsv-emacs"
                             (string-drop pkg-name
					  (string-length "emacs")))
	      pkg-name))))

;; TODO: ? emacs-buttercup-without-tests is not
(define-public emacs-buttercup-without-tests
  (package
   (inherit emacs-buttercup)
   (name "emacs-buttercup-without-tests")
   (arguments
    (substitute-keyword-arguments (package-arguments emacs-buttercup)
				  ((#:emacs emacs)
				   `,devmsv-emacs)
				  ((#:emacs emacs-next)
				   `,devmsv-emacs)
				  ((#:emacs emacs-minimal)
				   `,devmsv-emacs-minimal)
				  ((#:emacs emacs-next-minimal)
				   `,devmsv-emacs-minimal)))))

;; Returns #t if EMACS-PKG is not excluded from re-compilation.
(define (include-devmsv-emacs emacs-pkg)
  (let ((pkg-name (package-name emacs-pkg))
	(pkg-inputs (append (map (lambda (ele) (package-name (cadr ele)))
				 (package-inputs emacs-pkg))
			    (map (lambda (ele) (package-name (cadr ele)))
				 (package-propagated-inputs emacs-pkg))
			    (map (lambda (ele) (package-name (cadr ele)))
				 (package-native-inputs emacs-pkg))
			    (list (package-name emacs-pkg)))))
    (if (member #t (map
		    (lambda (ele)
		      (if (member ele devmsv-emacs-pkg-excludes)
			  #t
			  #f))
		    pkg-inputs))
	#f
	#t)))

(define-public (package-with-devmsv-emacs emacs-pkg)
  (if (include-devmsv-emacs emacs-pkg)
      ((package-input-rewriting `((,emacs unquote devmsv-emacs)
				  (,emacs-next unquote devmsv-emacs)
				  (,emacs-minimal unquote devmsv-emacs-minimal)
				  (,emacs-next-minimal unquote devmsv-emacs-minimal)
				  (,emacs-no-x unquote devmsv-emacs-no-x)
				  (,emacs-exwm unquote devmsv-emacs-exwm)
				  (,emacs-buttercup unquote emacs-buttercup-without-tests))
				devmsv-emacs-rename)
       (package
	(inherit emacs-pkg)
	(arguments
	 (substitute-keyword-arguments (package-arguments emacs-pkg)
				       ((#:emacs emacs)
					`,devmsv-emacs)
				       ((#:emacs emacs-next)
					`,devmsv-emacs)
				       ((#:emacs emacs-minimal)
					`,devmsv-emacs-minimal)
				       ((#:emacs emacs-next-minimal)
					`,devmsv-emacs-minimal)))))
      emacs-pkg))

(define emacs-next-xelb-no-x-toolkit
  (package-with-emacs-next emacs-xelb-no-x-toolkit))

(define emacs-xelb-no-x-toolkit-next
  (deprecated-package "emacs-xelb-no-x-toolkit-next"
                      emacs-next-xelb-no-x-toolkit))

(define-public emacs-next-exwm-no-x-toolkit
  (package-with-emacs-next emacs-exwm-no-x-toolkit))

;; -git packages reference current version to check if building form git causes some trouble
(define-public emacs-xelb-git
  (package
    (inherit ((options->transformation '((with-source . "emacs-xelb=https://github.com/emacs-exwm/xelb")))
              emacs-xelb))
    (name "emacs-xelb-git")))

(define-public emacs-exwm-git
  (package
    (inherit ((options->transformation '((with-source . "emacs-exwm=https://github.com/emacs-exwm/exwm")))
              emacs-exwm))
    (name "emacs-exwm-git")))

(define-public emacs-xelb-master
  (package
    (inherit ((package-input-rewriting `((,xcb-proto unquote xcb-proto-1.17)
                                         (,libxcb unquote libxcb-1.17)))
              ((options->transformation '((with-git-url . "emacs-xelb=https://github.com/emacs-exwm/xelb")))
               emacs-xelb)))
    (name "emacs-xelb-master")))

;; (define-public emacs-master-xelb-master
;;   (package-with-emacs-master emacs-xelb-master))

(define-public emacs-exwm-master
  (package
    (inherit ((options->transformation '((with-source . "emacs-exwm@master=https://github.com/emacs-exwm/exwm")))
              emacs-exwm))
    (name "emacs-exwm-master")))

;; (define-public emacs-master-exwm-master
;;   (package-with-emacs-master emacs-exwm-master))

(define-public emacs-exwm-xwidgets
  (package
    (inherit emacs-exwm)
    (name "emacs-xwidgets")
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-exwm)
       ((#:emacs emacs) `,emacs-xwidgets)))))

(define-public emacs-guix-master
  (package
    (inherit ((options->transformation '((with-branch . "emacs-guix=master")))
              emacs-guix))
    (name "emacs-guix-master")))

;; (define-public emacs-master-guix-master
;;   (package-with-emacs-master emacs-guix-master))

;; ice-9/boot-9.scm:1685:16: In procedure raise-exception:
;; In procedure struct-ref/immediate: Wrong type argument in position 1 (expecting struct): #<unspecified>
;; (define-public devmsv-emacs-guix
;;   (package
;;     (inherit (package-with-devmsv-emacs emacs-guix-master))
;;     (name "devmsv-emacs-guix")))

;;; packages

(define-public emacs-org-msg-master
  (package
   (inherit ((options->transformation '((with-branch . "emacs-org-msg=master")))
             emacs-org-msg))
   (name "emacs-org-msg-master")
   (version "master")))

(define-public emacs-spacious-padding
  (package
    (name "emacs-spacious-padding")
    (version "0.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://elpa.gnu.org/packages/spacious-padding-"
                           version ".tar"))
       (sha256
        (base32 "0x5bsyd6b1d3bzrsrpf9nvw7xj5ch114m2dilq64bg8y2db3452z"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/protesilaos/spacious-padding")
    (synopsis "Increase the padding/spacing of frames and windows")
    (description
     "This package provides a global minor mode to increase the spacing/padding of Emacs
windows and frames.  The idea is to make editing and reading feel more
comfortable.  Backronyms: Space Perception Adjusted Consistently Impacts Overall Usability State ...  padding; Spacious ...  Precise Adjustments to Desktop Divider
Internals Neatly Generated.")
    (license gpl3+)))

(define-public emacs-enwc
  (package
    (name "emacs-enwc")
    (version "2.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://elpa.gnu.org/packages/enwc-" version
                           ".tar"))
       (sha256
        (base32 "0y8154ykrashgg0bina5ambdrxw2qpimycvjldrk9d67hrccfh3m"))))
    (build-system emacs-build-system)
    (home-page "https://savannah.nongnu.org/p/enwc")
    (synopsis "The Emacs Network Client")
    (description
     "ENWC is the Emacs Network Client.  It is designed to provide a front-end to
various network managers, such as @code{NetworkManager} and Wicd.  Currently,
only @code{NetworkManager} and Wicd are supported, although experimental support
exists for Connman.  In order to use this package, add (setq
enwc-default-backend BACKEND-SYMBOL) where BACKEND-SYMBOL is either wicd or nm,
to your .emacs file (or other init file).  Then you can just run `enwc to start
everything.  Example: (setq enwc-default-backend nm).")
    (license gpl3+)))

(define-public emacs-bicycle
  (package
    (name "emacs-bicycle")
    (version "20220422.1600")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/tarsius/bicycle.git")
             (commit "d24aca41914d5ec20b8abfad1779bd0a4e908930")))
       (sha256
        (base32 "0knl1r856pvwklgcv4fpprw4ffi0yai7jvabg81cx8rv23gcxn3w"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-compat))
    (home-page "https://github.com/tarsius/bicycle")
    (synopsis "Cycle outline and code visibility")
    (description
     "This package provides commands for cycling the visibility of outline sections
and code blocks.  These commands are intended to be bound in
`outline-minor-mode-map and do most of the work using functions provided by the
`outline package.  This package is named `bicycle because it can additionally
make use of the `hideshow package.  If `hs-minor-mode is enabled and point is at
the start of a code block, then `hs-toggle-hiding is used instead of some
`outline function.  When you later cycle the visibility of a section that
contains code blocks (which is done using `outline functions), then code block
that have been hidden using `hs-toggle-hiding', are *not* extended.")
    (license gpl3+)))

(define-public emacs-kdeconnect
  (package
    (name "emacs-kdeconnect")
    (version "20231029.2250")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/carldotac/kdeconnect.el.git")
             (commit "2548bae3b79df23d3fb765391399410e2b935eb9")))
       (sha256
        (base32 "1qfy9hav2gzp4p1ahf0lvxig047wk9z9jnnka198w8ii78il1r8l"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/carldotac/kdeconnect.el")
    (synopsis "An interface for KDE Connect")
    (description
     "This package provides helper functions to use the command line version of KDE
Connect, a bridge between Android devices and computers, without leaving the
comfort of Emacs.  It requires KDE Connect on your computer(s) and Android
device(s).  KDE Connect currently requires Linux on the desktop, but does not
require KDE.")
    (license gpl3+)))

(define-public emacs-outline-minor-faces
  (package
    (name "emacs-outline-minor-faces")
    (version "20220720.1144")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/tarsius/outline-minor-faces.git")
             (commit "e578e71780b84e9b428d92bca9e340d18852f951")))
       (sha256
        (base32 "1mkh2s9y7ql82w92s4q6kd36rvbv24wjgf0dnzd60x509r6c6km5"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-compat))
    (home-page "https://github.com/tarsius/outline-minor-faces")
    (synopsis "Headings faces for outline-minor-mode")
    (description
     "Unlike `outline-mode', `outline-minor-mode does not change the appearance of
headings to look different from comments.  This package defines the faces
`outline-minor-N', which inherit from the respective `outline-N faces used in
`outline-mode and arranges for them to be used in `outline-minor-mode'.")
    (license gpl3+)))

(define-public emacs-xwwp
  (package
    (name "emacs-xwwp")
    (version "20200917.643")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/canatella/xwwp.git")
             (commit "f67e070a6e1b233e60274deb717274b000923231")))
       (sha256
        (base32 "1ikhgi3gc86w7y3cjmw875c8ccsmj22yn1zm3abprdzbjqlyzhhg"))))
    (build-system emacs-build-system)
    (arguments
     '(#:include '("^xwwp.el$" "^xwwp-follow-link.el$"
                   "^xwwp-follow-link-ido.el$")
       #:exclude '()))
    (home-page "https://github.com/canatella/xwwp")
    (synopsis "Enhance xwidget webkit browser")
    (description
     "This package provides the common functionnality for other xwidget webkit plus
packages.  It provides the customize group and a framework to inject css and
javascript functions into an `xwidget-webkit session.")
    (license gpl3+)))

(define-public emacs-elgantt
  (package
    (name "emacs-elgantt")
    (version "20210412")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/legalnonsense/elgantt")
             (commit "1a3d4fbc088997ad925ea3e2b88d77ec19f9156b")))
       (sha256
        (base32 "1jb8d9iw4xdv2ch10kgk6zyfki8sb40acbl9wii3hqrh9zahgiaw"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-org-ql emacs-ts emacs-s emacs-dash))
    (home-page "https://github.com/legalnonsense/elgantt")
    (synopsis "A Gantt chart/calendar for Orgmode")
    (description
     "El Gantt creates a Gantt calendar from your orgmode files. It provides a
flexible customization system with the goal of being adaptable to? multiple
purposes. You can move dates, scroll forward and backward, jump to the
underlying org file, and customize the display.")
    (license gpl3+)))

(define-public emacs-nano-theme
  (let ((commit "8d190055b63222d92f65ce7194b6f4bd05ac8efe"))
    (package
      (name "emacs-nano-theme")
      (version "0.3.4")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/rougier/nano-theme")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "19lx325dh26hsi8cgdgpnpxjjygqyrqvgqyb21rf1cwxwm9pnshc"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/rougier/nano-theme")
      (synopsis "Emacs minor mode controlling mode line")
      (description
       "A consistent theme for GNU Emacs. The light theme is based on Material colors and the dark theme is based on Nord colors.")
      (license gpl3+))))

(define-public emacs-mini-modeline
  (package
    (name "emacs-mini-modeline")
    (version "20220910.1508")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/kiennq/emacs-mini-modeline.git")
             (commit "55ad82056dd26bbf60585574927ef28441d318d8")))
       (sha256
        (base32 "05zswpi1anv4fd09wkspfxm0kwfcvp97hmwwlhw41vb3mhqk6czk"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-dash))
    (home-page "https://github.com/kiennq/emacs-mini-modeline")
    (synopsis "Display modeline in minibuffer")
    (description
     "Display modeline in minibuffer.  With this we save one display line and also
don't have to see redundant information.")
    (license gpl3+)))

(define-public emacs-embark-consult
  (package
    (name "emacs-embark-consult")
    (version "20230320.1442")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/oantolin/embark.git")
             (commit "3ffb27a833d326ccf7e375bb9d94ebf4dc37fa77")))
       (sha256
        (base32 "08cd6dn3qpavilpsf3lnfqrc81hkpg1lac2p48v8n2m94mdlsx2q"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-embark emacs-consult))
    (arguments
     '(#:include '("^embark-consult.el$")
       #:exclude '()))
    (home-page "https://github.com/oantolin/embark")
    (synopsis "Consult integration for Embark")
    (description
     "This package provides integration between Embark and Consult.  The package will
be loaded automatically by Embark.  Some of the functionality here was
previously contained in Embark itself: - Support for consult-buffer, so that you
get the correct actions for each type of entry in consult-buffer's list. -
Support for consult-line, consult-outline, consult-mark and consult-global-mark,
so that the insert and save actions don't include a weird unicode character at
the start of the line, and so you can export from them to an occur buffer (where
occur-edit-mode works!).  Just load this package to get the above functionality,
no further configuration is necessary.  Additionally this package contains some
functionality that has never been in Embark: access to Consult preview from
auto-updating Embark Collect buffer that is associated to an active minibuffer
for a Consult command.  For information on Consult preview, see Consult's info
manual or its readme on GitHub.")
    (license (list gpl3+))))

(define-public emacs-piper
  (package
    (name "emacs-piper")
    (version "20221004")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/howardabrams/emacs-piper.git")
             (commit "ddaf7d70cc8fbdd01ce6b7970e81a546aaeeb585")))
       (sha256
        (base32 "1zn1ia5an0c9kj02m3lyl4zfxhb4vd2r45ylg3frgnfdrwbk9m4w"))
       (patches
        (list (plain-file "emacs-piper.patch"
		    "diff --git a/piper.el b/piper.el
index 1655327..6c5f59d 100644
--- a/piper.el
+++ b/piper.el
@@ -195,7 +195,7 @@ command. In the eshell, the command can follow the `piper'keyword.\"
             (script  (or (first (last progdir)) program))
             ((name ext) (split-string script \"\\.\"))
             (buffer-name (format \"*%s*\" name)))
-      (list name (or (getenv \"SHELL\") \"sh\") sh-parts buffer-name))))
+      (list name \"sh\" sh-parts buffer-name))))
 
 (defun piper--process-name-from-shell (command)
   \"Return a good name for a process based on COMMAND.\"
")))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-dash emacs-s emacs-f emacs-hydra))
    (home-page "https://gitlab.com/howardabrams/emacs-piper")
    (synopsis
     "Wrapper around existing Emacs functionality that creates an
interactive user interface to dealing with unstructured textual data similar
with how we transform data through pipes in the shell.")
    (description
     "Wrapper around existing Emacs functionality that creates an
interactive user interface to dealing with unstructured textual data similar
with how we transform data through pipes in the shell.")
    (license (list gpl3+))))

(define-public emacs-mini-popup
  (let* ((commit "4302627cebf225b99b1d135b19d5b559dbc91dda"))
    (package
      (name "emacs-mini-popup")
      (version commit)
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/minad/mini-popup.git")
               (commit commit)))
         (sha256
          (base32 "17cmhzcjhwhz5zrhdd3kwn4l5k9ndn70rglxr5f8gxghvdap4jam"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://github.com/minad/mini-popup")
      (synopsis "show minibuffer in a popup")
      (description "show minibuffer in a popup")
      (license (list gpl3+)))))

;; Probably not going to use this anymore

(define-public emacs-gvariant
  (package
    (name "emacs-gvariant")
    (version "20210507.1310")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/wbolster/emacs-gvariant.git")
             (commit "f2e87076845800cbaaeed67f175ad4e4a9c01e37")))
       (sha256
        (base32 "1m6gwplzps0hykzszh0vh4rs48hcfi99vxb4i870y46lq2y8x2xb"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-parsec))
    (home-page "https://github.com/wbolster/emacs-gvariant")
    (synopsis "GVariant (GLib) helpers")
    (description
     "This package provides helpers for GVariant strings.  The only public function is
‘gvariant-parse’, which parses a string into an elisp data structure.")
    (license (list bsd-3))))

(define-public emacs-gsettings
  (package
    (name "emacs-gsettings")
    (version "20210407.2045")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/wbolster/emacs-gsettings.git")
             (commit "9f9fb1fe946bbba46307c26355f355225ea7262a")))
       (sha256
        (base32 "1pq18kz6dqk45ib70sch38ql63smpv7s80ik478ajjysks3882rc"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-dash emacs-gvariant emacs-s))
    (home-page "https://github.com/wbolster/emacs-gsettings")
    (synopsis "GSettings (Gnome) helpers")
    (description "This package provides helpers for Gnome GSettings.")
    (license (list bsd-3))))

(define-public emacs-consult-notes
  (package
    (name "emacs-consult-notes")
    (version "20230401.1922")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/mclear-tools/consult-notes.git")
             (commit "941325a3484782017c27e3ffe5ddb3b9151b8740")))
       (sha256
        (base32 "100v7bvj2hwln0jfvwfmi6rlx69x1m06k0f2yxg7w3s79jjlspy7"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-consult emacs-s emacs-dash))
    (home-page "https://github.com/mclear-tools/consult-notes")
    (synopsis "Manage notes with consult")
    (description "Manage your notes with consult.")
    (license (list gpl3+))))

(define-public emacs-jiralib2
  (package
    (name "emacs-jiralib2")
    (version "20200520.2031")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/nyyManni/jiralib2.git")
             (commit "c21c4e759eff549dbda11099f2f680b78d7f5a01")))
       (sha256
        (base32 "0yrcc9yfz9gxkhizy03bpysl1wcdbk0m6fj9hkqw3kbgnsk25h4p"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-request emacs-dash))
    (home-page "https://github.com/nyyManni/jiralib2")
    (synopsis "JIRA REST API bindings to Elisp")
    (description
     "This file provides a programatic interface to JIRA. It provides access to JIRA
from other programs, but no user level functionality.")
    (license gpl3+)))

(define-public emacs-ox-jira
  (package
    (name "emacs-ox-jira")
    (version "20220423.1403")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/stig/ox-jira.el.git")
             (commit "00184f8fdef02a3a359a253712e8769cbfbea3ba")))
       (sha256
        (base32 "1zyq4d0fvyawvb3w6072zl4zgbnrpzmxlz2l731wqrgnwm0l80gy"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-org))
    (home-page "https://github.com/stig/ox-jira.el")
    (synopsis "JIRA Backend for Org Export Engine")
    (description
     "This module plugs into the regular Org Export Engine and transforms Org files to
JIRA markup for pasting into JIRA tickets & comments.  In an Org buffer, hit
`C-c C-e j j to bring up *Org Export Dispatcher* and export it as a JIRA buffer.
 I usually use `C-x h to mark the whole buffer, then `M-w to save it to the kill
ring (and global pasteboard) for pasting into JIRA issues.")
    (license #f)))

(define-public emacs-language-detection
  (package
    (name "emacs-language-detection")
    (version "20161123.1813")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url
              "https://github.com/andreasjansson/language-detection.el.git")
             (commit "54a6ecf55304fba7d215ef38a4ec96daff2f35a4")))
       (sha256
        (base32 "0p8kim8idh7hg9398kpgjawkxq9hb6fraxpamdkflg8gjk0h5ppa"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/andreasjansson/language-detection.el")
    (synopsis "Automatic language detection from code snippets")
    (description
     "Automatic programming language detection using pre-trained random forest
classifier.  Supported languages: * ada * awk * c * clojure * cpp * csharp * css
* dart * delphi * emacslisp * erlang * fortran * fsharp * go * groovy * haskell
* html * java * javascript * json * latex * lisp * lua * matlab * objc * perl *
php * prolog * python * r * ruby * rust * scala * shell * smalltalk * sql *
swift * visualbasic * xml Entrypoints: * language-detection-buffer - When called
interactively, prints the language of the current buffer to the echo area - When
called non-interactively, returns the language of the current buffer *
language-detection-string - Non-interactive function, returns the language of
its argument")
    (license #f)))

(define-public emacs-ejira
  (package
    (name "emacs-ejira")
    (version "1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/nyyManni/ejira.git")
             (commit "49cb61239b19bf13775528231d7d49656ec7a8bb")))
       (sha256
        (base32 "0c3jz8bmygsnwmff316n4nkjc0cvhv2g44jczsd8dywy2kgd9c51"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-jiralib2
                             emacs-s
                             emacs-f
                             emacs-dash
                             emacs-ox-jira
                             emacs-language-detection
                             emacs-helm))
    (home-page "https://github.com/nyyManni/ejira")
    (synopsis "Jira integration for emacs org-mode")
    (description "Jira integration for emacs org-mode")
    (license gpl3+)))

(define-public emacs-media-progress
  (package
    (name "emacs-media-progress")
    (version "20230701.911")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/jumper047/media-progress.git")
             (commit "9f5e5bfa2c187680bb737c2b069650407ab6230d")))
       (sha256
        (base32 "0k59rjgdchi98vhkbcwy3ymcx5279w1hnimp9gphg7x6lkxsv6ph"))))
    (build-system emacs-build-system)
    (arguments
     '(#:exclude '("^media-progress-dirvish.el$")))
    (home-page "https://github.com/jumper047/media-progress")
    (synopsis "Display position where media player stopped.")
    (description
     "Package gets information about viewing progress of the media files.  It parses
data, saved by media players - for now only mpv player supported.")
    (license gpl3+)))

(define-public emacs-media-progress-dirvish
  (package
    (inherit emacs-media-progress)
    (name "emacs-media-progress-dirvish")
    (propagated-inputs (list emacs-dirvish))
    (arguments
     '(#:include '("^media-progress-dirvish.el$")))
    (synopsis "Dirvish module for media-progress.")))

(define-public emacs-org-remark
  (package
    (name "emacs-org-remark")
    (version "1.2.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/nobiot/org-remark")
             (commit (string-append "v" version))))
       (sha256
        (base32 "0j9r3bxqrsmf7iaw4rpd2vsw9ljq6drvbwhpc213kv2fr8lkfizh"))
       (file-name (git-file-name name version))))
    (build-system emacs-build-system)
    (home-page "https://github.com/nobiot/org-remark")
    (synopsis
     "Org-remark lets you highlight and annotate text files and websites with using Org mode")
    (description
     "Highlight and annotate any text file. The highlights and
notes are kepted in an Org file as the plain text database. This lets you easily
manage your marginal notes and use the built-in Org fecilities on them –
e.g. create a sparse tree based on the category of the notes.  Have the same
highlighting and annotating functionality for websites when you use EWW to
browse them (new in latest GNU-devel ELPA and is planned to be part of v1.1.0.)
Create your your own highlighter pens with different colors, type (e.g. underline,
squiggle, etc. optionally with Org’s category for search and filter on your
highlights and notes)")
    (license (list gpl3+))))

(define-public emacs-org-modern-indent
  (package
    (name "emacs-org-modern-indent")
    (version "0.1.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/jdtsmith/org-modern-indent")
             (commit (string-append "v" version))))
       (sha256
        (base32 "0308shm58s38r4h7pj5wv5pa99ki3qxzwfrfdyvvarlchsnbsxza"))
       (file-name (git-file-name name version))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-compat))
    (home-page "https://github.com/jdtsmith/org-modern-indent/")
    (synopsis "Modern block styling with org-indent.")
    (description
     "org-modern provides a clean and efficient org style. The
blocks (e.g. source, example) are particularly nicely decorated. But when
org-indent is enabled, the block \"bracket\", which uses the fringe area, is
disabled.

This small package approximately reproduces the block styling of org-modern when
using org-indent. It can be used with or without org-modern.")
    (license (list gpl3+))))

(define-public emacs-nano-calendar
  (let ((commit "244f9acd2e8e3f06e607fd72b34ef2eeb20cc1e7"))
    (package
      (name "emacs-nano-calendar")
      (version "20230716")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/rougier/nano-calendar.git")
               (commit commit)))
         (sha256
          (base32 "0r8zlvkqg3w5bz2bv69bvxjcmpmm5dd2l5p8bgf69is47z46d8wb"))
         (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs (list emacs-s emacs-nano-theme))
      (home-page "https://github.com/rougier/nano-calendar/")
      (synopsis "Nicolas-Rougier NANO-calendar")
      (description
       "This library offers an alternative to calendar. It’s very
similar and offer only a few options, like the possibility to color day
according to the number of item in the org-agenda.")
      (license (list gpl3+)))))

(define-public emacs-shrface
  (package
    (name "emacs-shrface")
    (version "20230805.500")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/chenyanming/shrface.git")
             (commit "946154a05b14a270f80351d5b2a98b19aa6c5745")))
       (sha256
        (base32 "0qqvw7az7cr20kss7szvwqmbznqb160jg13akdxkiiksgzng0f90"))))
    (build-system emacs-build-system)
    (propagated-inputs (list emacs-org emacs-language-detection))
    (home-page "https://github.com/chenyanming/shrface")
    (synopsis "Extend shr/eww with org features and analysis capability")
    (description
     "This package extends `shr/eww' with org features and analysis capability.  It
can be used in `dash-docs', `eww', `nov.el', `mu4e', `anki.el', etc. -
Configurable org-like heading faces, headline bullets, item bullets, paragraph
indentation, fill-column, item bullet, versatile hyper
links(http/https/file/mailto/etc) face and so on. - Browse the internet or local
html file with `eww just like org mode. - Read dash docsets with `dash-docs and
the beauty of org faces. - Read epub files with `nov.el , just like org mode. -
Read html email with `mu4e , the same reading experience just like org mode
without formatting html to org file. - Switch/jump the headlines just like
org-mode in `eww and `nov.el with `imenu - Toggle/cycle the headlines just like
org-mode in `eww and `nov.el with `outline-minor-mode and
`org-cycle'/`org-shifttab - Enable indentation just like org-mode in `eww and
`nov.el with `org-indent-mode - Analysis capability: - Headline analysis: List
all headlines with clickable texts. - URL analysis: List all classified URL with
clickable texts. - Export HTML buffer to an org file using shr engine (no Pandoc
is needed).")
    (license gpl3+)))

(define-public emacs-consult-web
  (package
    (name "emacs-consult-web")
    (version "ec7054eb")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/armindarvish/consult-web")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0ns5rc87q0cjz2r9nh9awwzinfckdgg2mk19kmlf3jfmb1bixr5m"))))
    (build-system emacs-build-system)
    (arguments
     (list
      ;; Include Pywal interaction scripts.
      #:include #~(cons "^sources/" %default-include)))
    (propagated-inputs (list emacs-embark emacs-elfeed emacs-gptel
                             emacs-consult-notes))
    (home-page "https://github.com/armindarvish/consult-web")
    (synopsis
     "consult-web is a package for getting search results from one or several custom
sources (web search engines, AI assistants, elfeed database, org notes, …) directly in
Emacs minibuffer.")
    (description
     "This package provides wrappers and macros around consult, to make it easier for users to
get results from search engines, websites, AI assistants, etc. inside emacs minibuffer
completion. In other words, consult-web enables getting consult-style multi-source or
dynamically completed results in minibuffer but for search engines and
APIs (e.g. simmilar to consult-web but for runing a google search from within emacs
minibuffer). It provides a range of sources as examples, but the main idea here is to
remain agnostic of the source and provide the toolset to the users to define their own
sources similar to what consult does for local sources.")
    (license gpl3+)))
