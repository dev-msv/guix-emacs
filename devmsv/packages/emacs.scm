;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023-2024 Akib Azmain Turja <akib@disroot.org>
;;; Copyright © 2025 Moisés Simón <moises.simon@universaldx.com>

;;; This file is not part of GNU Guix.
;;;
;;; This file is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This file is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this file.  If not, see <http://www.gnu.org/licenses/>.

(define-module (devmsv packages emacs)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages linux)	;inotify-tools
  #:use-module (gnu packages xorg)	;libxaw
  ;; emacs-no-x
  #:use-module (gnu packages tls)	;gnutls
  #:use-module (gnu packages base)	;make-ld-wrapper
  #:use-module (gnu packages gcc)	;libgccjit
  #:use-module (gnu packages mail)	;mailutils
  #:use-module (gnu packages acl)	;acl
  ;;! emacs-no-x
  #:use-module (gnu packages)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix transformations)
  #:use-module (guix utils)
  #:use-module ((guix licenses)
                #:prefix license:))

(define emacs-master-revision "0")
(define emacs-master-commit "2a5725b99a7b8034ac51b603c1bd9c8099e4adb0")
(define emacs-master-hash "180ra9sfpabniwgxfls50waml3ap7m6wzd6ggc9hgqazg63siyn9")

(define emacs-master-igc-revision "0")
(define emacs-master-igc-commit "48909543bdc7cfb605034398f2757e8d01004aca")
(define emacs-master-igc-hash "16q5ajg8gyb33y2xm3j8qv6mk7hrwlhk8agpz2y0iw20biapidd9")

(define emacs-master-version "31.0.50")

(define-public libmps
  (package
    (name "libmps")
    (version "1.118.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Ravenbrook/mps")
             (commit (string-append "release-" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "078iv3fsz0dnfwb7g63apkvcksczbqfxrxm73k80jwnwca6pgafy"))))
    (build-system gnu-build-system)
    (arguments
     (list #:tests? #f
	   #:phases #~(modify-phases %standard-phases
                      (replace 'build
			(lambda* (#:key outputs #:allow-other-keys)
			  (invoke #$(cc-for-target) "-O2" "-c" "code/mps.c"))))))
    (home-page "https://www.ravenbrook.com/project/mps/")
    (synopsis "This is the Memory Pool System Kit -- a complete set of sources for using, modifying, and adapting the MPS.")
    (description "The Memory Pool System (MPS) is a very general, adaptable, flexible, reliable, and efficient memory management system. It permits the flexible combination of memory management techniques, supporting manual and automatic memory management, in-line allocation, finalization, weakness, and multiple concurrent co-operating incremental generational garbage collections. It also includes a library of memory pool classes implementing specialized memory management policies.")
    (license license:bsd-2)))

(define-public emacs-next-lucid
  (package
    (inherit emacs-next)
    (name "emacs-next-lucid")
    (synopsis
     "The extensible, customizable, self-documenting text-editor (with Lucid
toolkit)")
    (inputs (modify-inputs (package-inputs emacs-next)
              (delete "gtk+")
              (prepend inotify-tools libxaw)))
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-next)
       ((#:configure-flags flags
         #~'())
        #~(cons* "--with-x-toolkit=lucid"
                 #$flags))))))

(define-public emacs-master-minimal
  (package
    (inherit emacs-next-minimal)
    (name "emacs-master-minimal")
    (version (git-version emacs-master-version emacs-master-revision emacs-master-commit))
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://git.savannah.gnu.org/cgit/emacs.git/snapshot/emacs-"
             emacs-master-commit ".tar.gz"))
       (sha256 (base32 emacs-master-hash))
       (patches (origin-patches (package-source emacs-next-minimal)))))
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-next-minimal)
       ((#:phases phases)
        #~(modify-phases #$phases
            (delete 'validate-comp-integrity)
            (replace 'patch-program-file-names
              (lambda* (#:key inputs #:allow-other-keys)
                ;; Substitute "sh" command.
                (substitute* '("src/callproc.c" "lisp/term.el"
                               "lisp/htmlfontify.el"
                               "lisp/mail/feedmail.el"
                               "lisp/obsolete/pgg-pgp.el"
                               "lisp/obsolete/pgg-pgp5.el"
                               ;; "lisp/obsolete/terminal.el"
                               "lisp/org/ob-eval.el"
                               "lisp/textmodes/artist.el"
                               "lisp/progmodes/sh-script.el"
                               "lisp/textmodes/artist.el"
                               "lisp/htmlfontify.el"
                               "lisp/term.el")
                  (("\"/bin/sh\"")
                   (format #f "~s"
                           (search-input-file inputs "bin/sh"))))
                (substitute* '("lisp/gnus/mm-uu.el" "lisp/gnus/nnrss.el"
                               "lisp/mail/blessmail.el")
                  (("\"#!/bin/sh\\\n\"")
                   (format #f "\"#!~a~%\""
                           (search-input-file inputs "bin/sh"))))
                (substitute* '("lisp/jka-compr.el" "lisp/man.el")
                  (("\"sh\"")
                   (format #f "~s"
                           (search-input-file inputs "bin/sh"))))

                ;; Substitute "awk" command.
                (substitute* '("lisp/gnus/nnspool.el" "lisp/org/ob-awk.el"
                               "lisp/man.el")
                  (("\"awk\"")
                   (format #f "~s"
                           (search-input-file inputs "bin/awk"))))

                ;; Substitute "find" command.
                (substitute* '("lisp/gnus/gnus-search.el"
                               "lisp/obsolete/nnir.el"
                               "lisp/progmodes/executable.el"
                               "lisp/progmodes/grep.el"
                               "lisp/filecache.el"
                               "lisp/ldefs-boot.el"
                               "lisp/mpc.el")
                  (("\"find\"")
                   (format #f "~s"
                           (search-input-file inputs "bin/find"))))

                ;; Substitute "sed" command.
                (substitute* "lisp/org/ob-sed.el"
                  (("org-babel-sed-command \"sed\"")
                   (format #f "org-babel-sed-command ~s"
                           (search-input-file inputs "bin/sed"))))
                (substitute* "lisp/man.el"
                  (("Man-sed-command \"sed\"")
                   (format #f "Man-sed-command ~s"
                           (search-input-file inputs "bin/sed"))))

                (substitute* "lisp/doc-view.el"
                  (("\"(gs|dvipdf|ps2pdf|pdftotext)\"" all what)
                   (let ((replacement (false-if-exception (search-input-file
                                                           inputs
                                                           (string-append
                                                            "/bin/" what)))))
                     (if replacement
                         (string-append "\"" replacement "\"") all))))
                ;; match ".gvfs-fuse-daemon-real" and ".gvfsd-fuse-real"
                ;; respectively when looking for GVFS processes.
                (substitute* "lisp/net/tramp-gvfs.el"
                  (("\\(tramp-compat-process-running-p \"(.*)\"\\)" all
                    process)
                   (format #f "(or ~a (tramp-compat-process-running-p ~s))"
                           all
                           (string-append "." process "-real"))))))))))))

(define* (emacs->emacs-master emacs-pkg #:optional name #:key
                              (version (package-version emacs-master-minimal))
                              (source (package-source emacs-master-minimal)))
  (package
    (inherit emacs-pkg)
    (name (or name
              (and (string-prefix? "emacs" (package-name emacs-pkg))
                   (string-append "emacs-master"
                                  (string-drop (package-name emacs-pkg)
                                               (string-length "emacs"))))))
    (version version)
    (source source)
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-pkg)
       ((#:phases phases)
        #~(modify-phases #$phases (delete 'validate-comp-integrity)))))))

(define-public emacs-master-no-x (emacs->emacs-master emacs-no-x))

(define-public emacs-master (emacs->emacs-master emacs))

(define-public emacs-master-xwidgets (emacs->emacs-master emacs-xwidgets))

(define-public emacs-master-pgtk (emacs->emacs-master emacs-pgtk))

(define-public emacs-master-pgtk-xwidgets
  (emacs->emacs-master emacs-pgtk-xwidgets))

(define-public emacs-master-motif (emacs->emacs-master emacs-motif))

(define-public emacs-master-no-x-toolkit
  (emacs->emacs-master emacs-no-x-toolkit))

(define-public emacs-master-wide-int (emacs->emacs-master emacs-wide-int))

(define-public emacs-master-tree-sitter
  (deprecated-package "emacs-master-tree-sitter" emacs-master))

(define-public emacs-master-lucid
  (package
    (inherit emacs-master)
    (name "emacs-master-lucid")
    (synopsis
     "The extensible, customizable, self-documenting text-editor (with Lucid toolkit)")
    (inputs (modify-inputs (package-inputs emacs-master)
              (delete "gtk+")
              (prepend inotify-tools libxaw)))
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-master)
       ((#:configure-flags flags
         #~'())
        #~(cons* "--with-x-toolkit=lucid"
                 #$flags))))))

(define-public emacs-master-igc-minimal
  (package
    (inherit emacs-master-minimal)
    (name "emacs-master-igc-minimal")
    (version (git-version emacs-master-version emacs-master-igc-revision emacs-master-igc-commit))
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://git.savannah.gnu.org/cgit/emacs.git/snapshot/emacs-"
             emacs-master-igc-commit ".tar.gz"))
       (sha256 (base32 emacs-master-igc-hash))
       (patches
	(search-patches "emacs-disable-jit-compilation.patch"
			"emacs-next-exec-path.patch"
			"emacs-fix-scheme-indent-function.patch"
			"emacs-next-native-comp-driver-options.patch"
			"emacs-next-native-comp-fix-filenames-igc.patch"
			"emacs-native-comp-pin-packages.patch"
			"emacs-pgtk-super-key-fix.patch")
	;; (list (local-file "patches/emacs-next-native-comp-fix-filenames-igc.patch"))
	)))
    (inputs (modify-inputs (package-inputs emacs-master-minimal)
              (append libmps)))
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-master-minimal)
       ((#:configure-flags flags
         #~'())
        #~(cons* "--with-mps=yes"
                 #$flags))))))

(define-public emacs-master-igc-no-x
  (package/inherit emacs-no-x
    (name "emacs-master-igc-no-x")
    (synopsis "The extensible, customizable, self-documenting text editor (console only)")
    (version (git-version emacs-master-version emacs-master-igc-revision emacs-master-igc-commit))
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://git.savannah.gnu.org/cgit/emacs.git/snapshot/emacs-"
             emacs-master-igc-commit ".tar.gz"))
       (sha256 (base32 emacs-master-igc-hash))
       (patches (origin-patches (package-source emacs-master-minimal)))))
    (arguments
     (substitute-keyword-arguments (package-arguments emacs-no-x)
       ((#:configure-flags flags #~'())
        #~(cons* "--with-mps=yes" #$flags))
       ((#:phases phases)
        #~(modify-phases #$phases
            (delete 'validate-comp-integrity)
            (replace 'patch-program-file-names
              (lambda* (#:key inputs #:allow-other-keys)
                ;; Substitute "sh" command.
                (substitute* '("src/callproc.c" "lisp/term.el"
                               "lisp/htmlfontify.el"
                               "lisp/mail/feedmail.el"
                               "lisp/obsolete/pgg-pgp.el"
                               "lisp/obsolete/pgg-pgp5.el"
                               ;; "lisp/obsolete/terminal.el"
                               "lisp/org/ob-eval.el"
                               "lisp/textmodes/artist.el"
                               "lisp/progmodes/sh-script.el"
                               "lisp/textmodes/artist.el"
                               "lisp/htmlfontify.el"
                               "lisp/term.el")
                  (("\"/bin/sh\"")
                   (format #f "~s"
                           (search-input-file inputs "bin/sh"))))
                (substitute* '("lisp/gnus/mm-uu.el" "lisp/gnus/nnrss.el"
                               "lisp/mail/blessmail.el")
                  (("\"#!/bin/sh\\\n\"")
                   (format #f "\"#!~a~%\""
                           (search-input-file inputs "bin/sh"))))
                (substitute* '("lisp/jka-compr.el" "lisp/man.el")
                  (("\"sh\"")
                   (format #f "~s"
                           (search-input-file inputs "bin/sh"))))

                ;; Substitute "awk" command.
                (substitute* '("lisp/gnus/nnspool.el" "lisp/org/ob-awk.el"
                               "lisp/man.el")
                  (("\"awk\"")
                   (format #f "~s"
                           (search-input-file inputs "bin/awk"))))

                ;; Substitute "find" command.
                (substitute* '("lisp/gnus/gnus-search.el"
                               "lisp/obsolete/nnir.el"
                               "lisp/progmodes/executable.el"
                               "lisp/progmodes/grep.el"
                               "lisp/filecache.el"
                               "lisp/ldefs-boot.el"
                               "lisp/mpc.el")
                  (("\"find\"")
                   (format #f "~s"
                           (search-input-file inputs "bin/find"))))

                ;; Substitute "sed" command.
                (substitute* "lisp/org/ob-sed.el"
                  (("org-babel-sed-command \"sed\"")
                   (format #f "org-babel-sed-command ~s"
                           (search-input-file inputs "bin/sed"))))
                (substitute* "lisp/man.el"
                  (("Man-sed-command \"sed\"")
                   (format #f "Man-sed-command ~s"
                           (search-input-file inputs "bin/sed"))))

                (substitute* "lisp/doc-view.el"
                  (("\"(gs|dvipdf|ps2pdf|pdftotext)\"" all what)
                   (let ((replacement (false-if-exception (search-input-file
                                                           inputs
                                                           (string-append
                                                            "/bin/" what)))))
                     (if replacement
                         (string-append "\"" replacement "\"") all))))
                ;; match ".gvfs-fuse-daemon-real" and ".gvfsd-fuse-real"
                ;; respectively when looking for GVFS processes.
                (substitute* "lisp/net/tramp-gvfs.el"
                  (("\\(tramp-compat-process-running-p \"(.*)\"\\)" all
                    process)
                   (format #f "(or ~a (tramp-compat-process-running-p ~s))"
                           all
                           (string-append "." process "-real"))))))))))
    (inputs (modify-inputs (package-inputs emacs-no-x)
	      (append libmps)))))

(define-public emacs-next-no-x
  (emacs->emacs-next emacs-no-x))

(define-public emacs-master-lucid-igc
  (package
    (inherit emacs-master-igc-minimal)    
    (name "emacs-master-lucid-igc")
    (inputs (modify-inputs (package-inputs emacs-master-lucid)
	      (append libmps)))))

(define-public emacs-pretest-minimal
  (package
    (inherit emacs-next-minimal)
    (name "emacs-pretest-minimal")
    (version "30.0.93")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://git.savannah.gnu.org/git/emacs.git")
	     (commit (string-append "emacs-" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "05a587a7bnbz8ms45h4hs1c33j8268aymf3y1bpxipl3abi43jlm"))
       (patches
        (search-patches "emacs-next-exec-path.patch"
                        "emacs-fix-scheme-indent-function.patch"
                        "emacs-next-native-comp-driver-options.patch"
                        "emacs-pgtk-super-key-fix.patch"))))))

(define* (emacs-next->emacs-pretest emacs-pkg #:optional name)
  (package
    (inherit emacs-pkg)
    (name (or name
              (and (string-prefix? "emacs-next" (package-name emacs-pkg))
                   (string-append "emacs-pretest"
                                  (string-drop (package-name emacs-pkg)
                                               (string-length "emacs-next"))))))
    (version (package-version emacs-pretest-minimal))
    (source (package-source emacs-pretest-minimal))))

(define-public emacs-pretest
  (emacs-next->emacs-pretest emacs-next "emacs-pretest"))

(define-public emacs-pretest-lucid
  (emacs-next->emacs-pretest emacs-next-lucid "emacs-pretest-lucid"))

(define-public devmsv-emacs
  (package
    (inherit emacs-master-lucid-igc)
    (name "devmsv-emacs")))

(define-public devmsv-emacs-minimal
  (package
    (inherit emacs-master-lucid-igc)
    (name "devmsv-emacs-minimal")))

(define-public devmsv-emacs-no-x
  (package
    (inherit emacs-master-igc-no-x)
    (name "devmsv-emacs-no-x")))
